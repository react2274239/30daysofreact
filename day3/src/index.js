import React from 'react';
import ReactDOM from 'react-dom/client';
import scottImage from './images/Scott.png'

const root = ReactDOM.createRoot(document.getElementById('root'));

const userStyles = {
	height: "100px",
	widht: "100px",
	borderRadius: "999px"
};

const user = (
	<div>
		<img style={userStyles} src={scottImage} alt="Scott Image" />
	</div>
);

const headerStyles = {
	backgroundColor: '#61DBFB',
	fontFamily: 'Helvetica Neue',
	padding: 25,
	lineHeight: 1.5,
};

const header = (
	<header style={headerStyles}>
		<h1>Welcome to 30 Days of React</h1>
		<h2>Getting Started With React</h2>
		<h3>JavaScript Library</h3>
		<p>Scott Hasserd</p>
		{user}
		<small>3. October 2023</small>
	</header>
);

const mainStyles = {
	backgroundColor: "F3F0F5",
};

const techs = ["JavaScript", "HTML", "CSS"];
const techsFormatted = techs.map(tech => <li>{tech}</li>);

// JSX element, main
const main = (
  <main style={mainStyles}>
    <p>Prerequisite to get started{" "} 
	<strong><em>react.js</em></strong>:</p>
    <ul>
		{techsFormatted}
    </ul>
  </main>
)

const footerStyles = {
  backgroundColor: '#61DBFB',
};

// JSX element, footer
const footer = (
  <footer style={footerStyles}>
    <p>Copyright 2020</p>
  </footer>
)

// JSX element, app, a container or a parent
const app = (
  <div>
    {header}
    {main}
    {footer}
  </div>
)
root.render(app, root);
