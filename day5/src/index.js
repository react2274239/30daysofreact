import React from 'react';
import ReactDOM from 'react-dom/client';
import scottImage from './images/Scott.png'

const root = ReactDOM.createRoot(document.getElementById('root'));

const userStyles = {
	height: "100px",
	widht: "100px",
	borderRadius: "999px"
};

const userData = {
	firstName: "Scott",
	lastName: "Hasserd",
}

const User = (props)=>{
	return(
		<div className="user-card">
			<h2>{props.data.firstName} {props.data.lastName}</h2>
			<img style={userStyles} src={scottImage} alt="Scott Image" />
		</div>
	);
}

const Header = (props)=>{
	const {welcome, title, subtitle, author, date} = props;
	return (
		<header>
			<div className="header-wrapper">
				<h1>{welcome}</h1>
				<h2>{title}</h2>
				<h3>{subtitle}</h3>
				<p>{author}</p>
				<small>{date}</small>
			</div>
		</header>
	);
}

const techs = ["JavaScript", "HTML", "CSS"];
const techsFormatted = techs.map(tech => <li key={tech}>{tech}</li>);

const Button = (props)=> <button style={props.style} onClick={props.onClick}> action! </button>;

// JSX element, main
const Main = ()=>{

	let btnColor = "green";
	const hexaColor = ()=>{
		let str = "0123456789abcdef";
		let color = '';
		for(let i = 0; i < 6; i++){
			let index = Math.floor(Math.random() * str.length);
			color += str[index];
		}
		btnColor = '#' + color;
		console.log(btnColor);
		return btnColor;
	}

	const buttonStyles = {
		padding: "10px 20px",
		background: btnColor,
		border: "none",
		borderRadius: 5,
	};

	return(
	  <main>
		<div className="main-wrapper">
			<p>Prerequisite to get started{" "} 
			<strong><em>react.js</em></strong>:</p>
			<ul>
				{techsFormatted}
			</ul>
			<User data={userData} />
		</div>
		<Button style={buttonStyles} onClick={hexaColor} />
	  </main>
	)
}

// JSX element, footer
const Footer = ()=>(
  <footer>
	<div className="footer-wrapper">
    <p>Copyright 2020</p>
	</div>
  </footer>
)

// JSX element, app, a container or a parent
const App = () =>{
	const welcome = "Welcome to 30 Days of React";
	const title = "Getting Started With React";
	const subtitle = "JavaScript Library";
	const author = "Scott Hasserd";
	const date = "5. October 2023";
	return (
	  <div className="app">
		<Header 
			welcome={welcome}
			title={title}
			subtitle={subtitle}
			author={author}
			date={date}
		/>
		<Main />
		<Footer />
	  </div>
	)
}
root.render(<App />);
//root.render(<HexaColor />, root);
