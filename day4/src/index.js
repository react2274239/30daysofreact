import React from 'react';
import ReactDOM from 'react-dom/client';
import scottImage from './images/Scott.png'

const root = ReactDOM.createRoot(document.getElementById('root'));

const userStyles = {
	height: "100px",
	widht: "100px",
	borderRadius: "999px"
};

const User = ()=>(
	<div className="user-card">
		<h2>Scott Hasserd</h2>
		<img style={userStyles} src={scottImage} alt="Scott Image" />
	</div>
);

const Header = ()=>(
	<header>
		<div className="header-wrapper">
			<h1>Welcome to 30 Days of React</h1>
			<h2>Getting Started With React</h2>
			<h3>JavaScript Library</h3>
			<p>Scott Hasserd</p>
			<small>3. October 2023</small>
		</div>
	</header>
);

const techs = ["JavaScript", "HTML", "CSS"];
const techsFormatted = techs.map(tech => <li>{tech}</li>);

const buttonStyles = {
	padding: "10px 20px",
	background: "rgb(0, 255, 0)",
	border: "none",
	borderRadius: 5,
};

const Button = ()=> <button style={buttonStyles}> action! </button>;

const hexaColor = ()=>{
	let str = "0123456789abcdef";
	let color = '';
	for(let i = 0; i < 6; i++){
		let index = Math.floor(Math.random() * str.length);
		color += str[index];
	}
	return '#' + color;
}

const HexaColor = ()=> <div>{hexaColor()}</div>

// JSX element, main
const Main = ()=>(
  <main>
	<div className="main-wrapper">
		<p>Prerequisite to get started{" "} 
		<strong><em>react.js</em></strong>:</p>
		<ul>
			{techsFormatted}
		</ul>
		<User />
	</div>
	<Button />
  </main>
)

// JSX element, footer
const Footer = ()=>(
  <footer>
	<div className="footer-wrapper">
    <p>Copyright 2020</p>
	</div>
  </footer>
)

// JSX element, app, a container or a parent
const App = () => (
  <div className="app">
	<Header />
    <Main />
    <Footer />
  </div>
)
root.render(<App />, root);
//root.render(<HexaColor />, root);
