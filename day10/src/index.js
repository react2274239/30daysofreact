import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.js'

const root = ReactDOM.createRoot(document.getElementById('root'));


// JSX element, app, a container or a parent
root.render(<App />);
//root.render(<HexaColor />, root);
