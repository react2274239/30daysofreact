import React from 'react';
import ReactDOM from 'react-dom/client';
import scottImage from './images/Scott.png'

const root = ReactDOM.createRoot(document.getElementById('root'));

const userStyles = {
	height: "100px",
	widht: "100px",
	borderRadius: "999px"
};

const User = (props) => {
	return (
		<div className="user-card">
			<h2>{props.userData.firstName} {props.userData.lastName}</h2>
			<img style={userStyles} src={scottImage} alt="Scott Image" />
		</div>
	);
}

class Header extends React.Component {
	greetPeople = () => {
		alert("Welcome to 30 Days of React!");
	}
	render() {
		const { welcome, title, subtitle, author, date } = this.props;
		return (
			<header>
				<div className="header-wrapper">
					<h1>{welcome}</h1>
					<h2>{title}</h2>
					<h3>{subtitle}</h3>
					<p>{author}</p>
					<small>{date}</small><br />
					<button onClick={this.greetPeople}>Greet</button>
				</div>
			</header>
		);
	}
}

const skills = [
	["HTML", 10],
	["CSS", 7],
	["JavaScript", 9],
	["React", 8],
];

const Skill = ({ skill: [tech, level] }) => {
	return (
		<li>
			{tech} {level}
		</li>
	)
}

const Skills = ({ skills }) => {
	const skillsList = skills.map(skill => <Skill key={skill} skill={skill} />);
	return <div className="container"><ul>{skillsList}</ul></div>
}
// CSS styles in JavaScript Object
const buttonStyles = {
	backgroundColor: '#61dbfb',
	padding: 10,
	border: 'none',
	borderRadius: 5,
	margin: 3,
	cursor: 'pointer',
	fontSize: 18,
	color: 'white',
}
const Button = (props) => <button style={props.style} onClick={props.onClick}> {props.text} </button>;

const Count = ({ count, addOne, minusOne }) => (
	<div>
		<h3>{count} </h3>
		<div>
			<Button style={buttonStyles} text="+1" onClick={addOne} />
			<Button style={buttonStyles} text="-1" onClick={minusOne} />
		</div>
	</div>
)

// JSX element, main
class Main extends React.Component {
	render() {
		const {
			techsFormatted,
			userData,
			skills,
			changeBackground,
			count,
			addOne,
			minusOne,
			styles,
		} = this.props
		return (
			<main style={styles}>
				<div className="main-wrapper">
					<p>Prerequisite to get started{" "}
						<strong><em>react.js</em></strong>:</p>
					<ul>
						{techsFormatted}
					</ul>
					<User userData={userData} />
					<Count count={count} addOne={addOne} minusOne={minusOne} />
					<Button style={buttonStyles} text="Change" onClick={changeBackground} />
				</div>
				<h2>Skills</h2>
				<Skills skills={skills} />
			</main>
		)
	}
}

// JSX element, footer
const Footer = () => (
	<footer>
		<div className="footer-wrapper">
			<p>Copyright 2020</p>
		</div>
	</footer>
)

// JSX element, app, a container or a parent
class App extends React.Component {
	state = {
		count: 0,
		styles: {
			backgroundColor: '',
			color: '',
		}
	}

	addOne = () => {
		this.setState({ count: this.state.count + 1 });
	}

	minusOne = () => {
		this.setState({ count: this.state.count - 1 });
	}

	changeBackground = () => {
		if (this.state.styles.color === '')
			this.setState({
				styles: {
					backgroundColor: "darkblue",
					color: "white",
				}
			});
		else
			this.setState({
				styles: {
					backgroundColor: "",
					color: "",
				}
			});
	}

	render() {
		const welcome = "Welcome to 30 Days of React";
		const title = "Getting Started With React";
		const subtitle = "JavaScript Library";
		const author = "Scott Hasserd";
		const date = "5. October 2023";
		const userData = {
			firstName: "Scott",
			lastName: "Hasserd",
		}

		const techs = ["JavaScript", "HTML", "CSS"];
		const techsFormatted = techs.map(tech => <li key={tech}>{tech}</li>);

		return (
			<div className="app">
				<Header
					welcome={welcome}
					title={title}
					subtitle={subtitle}
					author={author}
					date={date}
				/>
				<Main
					styles={this.state.styles}
					userData={userData}
					skills={skills}
					techsFormatted={techsFormatted}
					addOne={this.addOne}
					minusOne={this.minusOne}
					count={this.state.count}
					changeBackground={this.changeBackground}
				/>
				<Footer />
			</div>
		)
	}
}
root.render(<App />);
//root.render(<HexaColor />, root);
