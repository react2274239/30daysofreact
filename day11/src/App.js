import React from 'react'
import Header from './components/header/Header.js'
import Footer from './components/footer/Footer.js'
import Main from './components/main/Main.js'
import Login from './components/login/Login.js'

const skills = [
	["HTML", 10],
	["CSS", 7],
	["JavaScript", 9],
	["React", 8],
];

class App extends React.Component {
	state = {
		count: 0,
		loggedIn: false,
		styles: {
			backgroundColor: '',
			color: '',
		}
	}

	addOne = () => {
		this.setState({ count: this.state.count + 1 });
	}

	minusOne = () => {
		this.setState({ count: this.state.count - 1 });
	}

	changeBackground = () => {
		if (this.state.styles.color === '')
			this.setState({
				styles: {
					backgroundColor: "darkblue",
					color: "white",
				}
			});
		else
			this.setState({
				styles: {
					backgroundColor: "",
					color: "",
				}
			});
	}

	login = () => {
		this.setState({loggedIn: !this.state.loggedIn})
	}

	render() {
		const welcome = "Welcome to 30 Days of React";
		const title = "Getting Started With React";
		const subtitle = "JavaScript Library";
		const author = "Scott Hasserd";
		const date = "5. October 2023";
		const userData = {
			firstName: "Scott",
			lastName: "Hasserd",
		}
		let status;

		if (this.state.loggedIn)
			status = <h4>You are logged in!</h4>
		else
			status = <h4>Please login!</h4>

		const techs = ["JavaScript", "HTML", "CSS"];
		const techsFormatted = techs.map(tech => <li key={tech}>{tech}</li>);

		return (
			<div className="app">
				<Header
					welcome={welcome}
					title={title}
					subtitle={subtitle}
					author={author}
					date={date}
				/>
				{status}
				<Login onClick={this.login} />
				{this.state.loggedIn && <Main
					styles={this.state.styles}
					userData={userData}
					skills={skills}
					techsFormatted={techsFormatted}
					addOne={this.addOne}
					minusOne={this.minusOne}
					count={this.state.count}
					changeBackground={this.changeBackground}
				/>}
				<Footer />
			</div>
		)
	}
}

export default App;
