import React from 'react';
import scottImage from '../../images/Scott.png'
import Keycode from '../keycode/Keycode'

const userStyles = {
	height: "100px",
	widht: "100px",
	borderRadius: "999px"
};

const User = (props) => {
	return (
		<div className="user-card">
			<h2>{props.userData.firstName} {props.userData.lastName}</h2>
			<img style={userStyles} src={scottImage} alt="Scott Image" />
		</div>
	);
}

const Skill = ({ skill: [tech, level] }) => {
	return (
		<li>
			{tech} {level}
		</li>
	)
}

const Skills = ({ skills }) => {
	const skillsList = skills.map(skill => <Skill key={skill} skill={skill} />);
	return <div className="container"><ul>{skillsList}</ul></div>
}
// CSS styles in JavaScript Object
const buttonStyles = {
	backgroundColor: '#61dbfb',
	padding: 10,
	border: 'none',
	borderRadius: 5,
	margin: 3,
	cursor: 'pointer',
	fontSize: 18,
	color: 'white',
}
export const Button = (props) => <button style={props.style} onClick={props.onClick}> {props.text} </button>;

const Count = ({ count, addOne, minusOne }) => (
	<div>
		<h3>{count} </h3>
		<div>
			<Button style={buttonStyles} text="+1" onClick={addOne} />
			<Button style={buttonStyles} text="-1" onClick={minusOne} />
		</div>
	</div>
)

// JSX element, main
class Main extends React.Component {
	render() {
		const {
			techsFormatted,
			userData,
			skills,
			changeBackground,
			count,
			addOne,
			minusOne,
			styles,
		} = this.props
		return (
			<main style={styles}>
				<div className="main-wrapper">
					<p>Prerequisite to get started{" "}
						<strong><em>react.js</em></strong>:</p>
					<ul>
						{techsFormatted}
					</ul>
					<User userData={userData} />
					<Keycode />
					<Count count={count} addOne={addOne} minusOne={minusOne} />
					<Button style={buttonStyles} text="Change" onClick={changeBackground} />
				</div>
				<h2>Skills</h2>
				<Skills skills={skills} />
			</main>
		)
	}
}

export default Main;
