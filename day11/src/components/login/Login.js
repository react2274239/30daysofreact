import React from 'react'
import {Button} from '../main/Main.js'

const Login = (props) => {
	return <Button onClick={props.onClick} text="Login/Logout" />
}

export default Login;
