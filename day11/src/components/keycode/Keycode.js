import React,{Component} from 'react'

class Keycode extends Component {
	state = {keycode:0}

	getKeycode = (e) =>{
		console.log(e.keyCode)
		this.setState({keycode: e.keyCode});
	};

	render(){
		return (
			<>
				<h3>Key: {this.state.keycode}</h3>
				<input type="text" onKeyUp={this.getKeycode} />
			</>
		);
	};
}
export default Keycode;
