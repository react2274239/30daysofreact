import React from 'react'

class Header extends React.Component {
	greetPeople = () => {
		alert("Welcome to 30 Days of React!");
	}
	render() {
		const { welcome, title, subtitle, author, date } = this.props;
		return (
			<header>
				<div className="header-wrapper">
					<h1>{welcome}</h1>
					<h2>{title}</h2>
					<h3>{subtitle}</h3>
					<p>{author}</p>
					<small>{date}</small><br />
					<button onClick={this.greetPeople}>Greet</button>
				</div>
			</header>
		);
	}
}

export default Header;
